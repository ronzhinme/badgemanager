
#include "BadgemanWin.h"
#include <stdio.h>

BadgemanWin::BadgemanWin()
{
    CoInitializeEx(NULL, COINIT_MULTITHREADED);
    HRESULT hr = CoCreateInstance(CLSID_TaskbarList, NULL, CLSCTX_ALL, IID_ITaskbarList3, (LPVOID *)&pTaskbar);
    pTaskbar->HrInit();
}

BadgemanWin::~BadgemanWin()
{
    pTaskbar->Release();
    pTaskbar = NULL;
    CoUninitialize();
}

void BadgemanWin::setBadge(HWND hwnd, const char *iconFilename, const wchar_t *text)
{
    if (hwnd == NULL)
        return;

    HICON hicon = (HICON)LoadImage( // returns a HANDLE so we have to cast to HICON
        NULL,                       // hInstance must be NULL when loading from a file
        (LPCSTR)iconFilename,       // the icon file name
        IMAGE_ICON,                 // specifies that the file is an icon
        0,                          // width of the image (we'll specify default later on)
        0,                          // height of the image
        LR_LOADFROMFILE |           // we want to load a file (as opposed to a resource)
            LR_DEFAULTSIZE |        // default metrics based on the type (IMAGE_ICON, 32x32)
            LR_SHARED               // let the system release the handle when it's no longer used
    );
    if (hicon == NULL)
    {
        printf("icon ERROR\n");
        return;
    }

    pTaskbar->SetOverlayIcon(hwnd, hicon, text);
}

void BadgemanWin::setProgressValue(HWND hwnd, unsigned long currentValue, unsigned long maxValue)
{
    if (hwnd == NULL)
        return;

    pTaskbar->SetProgressValue(hwnd, currentValue, maxValue);
}

void BadgemanWin::setProgressStatus(HWND hwnd, TBPFLAG statusValue)
{
    if (hwnd == NULL)
        return;

    pTaskbar->SetProgressState(hwnd, statusValue);
}