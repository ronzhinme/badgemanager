
#ifndef BADGEMANWIN_H
#define BADGEMANWIN_H

#include <windows.h>
#include <shobjidl_core.h>

// Windows 7 SDK required
#ifdef __ITaskbarList3_INTERFACE_DEFINED__

class BadgemanWin
{
public:
    BadgemanWin();
    ~BadgemanWin();

    void setBadge(HWND hwnd, const char *iconFilename, const wchar_t *text);
    void setProgressValue(HWND hwnd, unsigned long currentValue, unsigned long maxValue);
    void setProgressStatus(HWND hwnd, TBPFLAG statusValue);
    
private:
    ITaskbarList3 *pTaskbar = NULL;
};

#endif
#endif