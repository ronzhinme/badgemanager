# BadgeManager
BadgeManager assists you to apply an extra icon for the application's icon at the task bar.  

# Windows:
On Windows platform you can:
- setBadge to set icon and text on it
- setProgressValue to set current value of the progress bar
- setProgressStatus to set the current progess bar's status
  
![](./badgeManager_at_work_windows.png)
At this picture:
- green, yellow or red progress bar on application icon. 
  Color depends on status of setProgressStatus
  Value depends on value of setProgressValue
- small extra icon. 
  It may be used to show the extended info of the application for example: new message received or application's extra status
   
# MacOS:
