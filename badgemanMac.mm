#include "badgemanMac.h"
#import <Cocoa/Cocoa.h>

void BadgemanMac::setBadgeLabel(const std::string & val)
{
    NSString * badgeString = [NSString stringWithCString:val.c_str() encoding:[NSString defaultCStringEncoding]];
    [[NSApp dockTile] setBadgeLabel: badgeString];
    [badgeString release];
}
