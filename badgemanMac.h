#ifndef BADGEMANWIN_H
#define BADGEMANWIN_H

#include <string>

class BadgemanMac
{
public:
    static void setBadgeLabel(const std::string & val);
};

#endif