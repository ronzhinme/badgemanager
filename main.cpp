
#include <iostream>

#if defined(_WIN32)
#include <windows.h>
#include "badgemanWin.h"
#elif defined(__APPLE__)
#include "badgemanMac.h"
#elif defined(__linux__)
#endif

#if defined(_WIN32)
bool testWin()
{
        HWND wndId = GetForegroundWindow();
        if (wndId == NULL)
        {
                std::cout << "wndId == NULL" << std::endl;
                return false;
        }

        BadgemanWin bm;
        bm.setBadge(wndId, "test.ico", L"_+_");
        for(auto i =0; i < 10000; ++i)
        {
                bm.setProgressValue(wndId, i, 10000);
                bm.setProgressStatus(wndId, (TBPFLAG)(i & 0xF));
                Sleep(1);
        }
        bm.setProgressValue(wndId, 0, 0);

        int i;
        std::cin >> i;
        return true;
}
#elif defined(__APPLE__)
bool testMac()
{
        BadgemanMac::setBadgeLabel("Test");        
        int i;
        std::cin >> i;
        return true;
}
#elif defined(__linux__)
bool testLinux()
{
        return true;
}
#endif

int main()
{
#if defined(_WIN32)
        return !testWin();
#elif defined(__APPLE__)
        return testMac();
#elif defined(__linux__)
        return testLinux();
#endif
return 0;
}